# sst-apollo

Trying out the serverless stack (SST) framework with an Apollo/Typescript

## Setup/Installation
* Copy the `.env` file to `.env.local` and make required changes
  * Changes to `.env.local` will overwrite the values in `.env` and won't be tracked by git
* From the `graphql-apollo` directory:
  * `npm install`

## Running "Locally"
* From the `graphql-apollo` directory: `npm start`
  * This does require AWS credentials to be setup
  * It basically uses a live Lambda development environment
  * A noticeable difference between SST and serverless, is that SST is able to use my default `aws-vault` setup, while I wasn't able to with serverless

### Accessing
* Apollo can be accessed directly through the provided API Gateway link
* Can also be accessed through the SST console: `https://console.serverless-stack.com/`
  * GraphQL should be an available tab to access Apollo as well

#### Sample Query:
```
query {
  hello
}
```

### Modifying
* If you have `npm start` running, just modify the source files and it seems to automagically get deployed
  * Assumed that it wasn't working because there wasn't any output, but confirmed that the page does get uploaded

### Removing
* `npx sst remove`
  * Defaults to `local`

## Deploying to AWS
* `npx sst deploy --stage dev`
  * Defaults to `dev`
  * There is a check for a stage named `prod` for production

### Removing
* `npx sst remove --stage dev`

## Commands Used to Setup This Repository
Shouldn't have to be ran, but may be useful

* Creating the SST app:
  * `npm init sst typescript-starter graphql-apollo`
    * When I tried to run this with an older version of Node/npm I received an error about not being able to find the `fs` module
      * I upgrade to Node version `16.15.1` and npm version `8.11.0` and this seemed to resolve the issue

## References / Inspiration
* [Deploying Apollo with SST](https://serverless-stack.com/examples/how-to-create-an-apollo-graphql-api-with-serverless.html)
* [Adding Custom Domain to SST](https://serverless-stack.com/examples/how-to-add-a-custom-domain-to-a-serverless-api.html)
* [Installing SST](https://docs.serverless-stack.com/installation)
* [Comparison of serverless and sst](https://serverless-stack.com/resources/serverless-framework-vs-sst.html)
* [Article on SST](https://dev.to/julbrs/sst-the-most-underrated-serverless-framework-you-need-to-discover-25ne)