import { GraphQLApi, StackContext } from "@serverless-stack/resources";

export function MyStack({ stack, app }: StackContext) {
  const stage = app.stage;
  const domain_name = process.env.DOMAIN_NAME;
  const project_name = process.env.PROJECT_NAME;
  const sub_domain = (stage == "prod") ? project_name : `${project_name}-${stage}`;

  // Create the GraphQL API
  const api = new GraphQLApi(stack, "ApolloApi", {
    customDomain: {
      domainName: `${sub_domain}.${domain_name}`,
      hostedZone: `${domain_name}`,
    },
    server: {
      handler: "functions/lambda.handler",
      bundle: {
        format: "cjs",
      },
    },
  });

  // Show the API endpoint in output
  stack.addOutputs({
    ApiEndpoint: api.url,
  });
}